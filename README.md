# VR_Rendering

This project is a work in progress and not fully optimized yet.
## Prerequisites



- **Unity 2022.3.7 or higher** - This project requires at least Unity version 2022.3.7. 

- **Blender 4.0 or higher** - Blender is required for handling certain assets in this project. 




### Missing Meshes in the Living Room Scene

If you notice that some meshes are missing in the living room scene:

- **Reimport the "DemoRoom" Blend File**



## License

This project is licensed under the CC0 license

## Acknowledgments

I would like to acknowledge the following sources for the assets used in this project:

- [Polyhaven.com](https://polyhaven.com)
- [AmbientCG.com](https://ambientcg.com)
- [Blendswap.com](https://blendswap.com)
- [Sharetextures.com](https://sharetextures.com)
- [Trashbyte](https://trashbyte.io)
- [alstrainfinite.itch.io](https://alstrainfinite.itch.io)
- [Blendfile.com](https://blendfile.com)
- [ItchIO](https://itch.io)
- [CCOTextures](https://ccotextures.com)
- [blenderboom.com](https://blenderboom.com)
- [Blenderborn.com](https://blenderborn.com)
- [Freepoly.org](https://freepoly.org)
